# ECS Tutorial with Java and Artemis-odb

This projects demonstrates the use of an Entity-Component-System (ECS) for the implementation of an individual-based model (IBM). We use Java and the ECS library [Artemis-odb](<https://github.com/junkdog/artemis-odb>) to build a simple grassing model.

The complete model code is presented along with the descriptions. ECS-specific remarks are shown as indented notes:

> ECS-remarks look like this.

**Contents**

* [How to use this project?](#how-to-use-this-project)
* [What is an Entity-Component-System?](#what-is-an-entity-component-system)
* [Model purpose](#model-purpose)
* [Entities, state variables and scales](#entities-state-variables-and-scales)
* [Process overview and scheduling](#process-overview-and-scheduling)
* [Design details](#design-details)
* [Initialization](#initialization)
* [Submodels](#submodels)
* [References](#references)
* [Appendix](#appendix)

<img title="Screenshot of the model" src="https://git.ufz.de/oesa/ecs-tutorial/uploads/78a0b08e3e5d4f64383337735f133b43/image.png" alt="Screenshot of the model" width="400">

*Screenshot of the model*

## How to use this project?

This document contains the most important information explaining the basics of an ECS. It is recommended that you read this document from top to bottom, as this is probably the most effective way to absorb the content.

The model description is structured following the ODD protocol (Grimm et al. 2006, 2010) to demonstrate the good fit of ECS and ODD.

See the **[How to](md/howto.md)** for details on further usage, from simply running downloaded binaries to cloning and building it on your machine.

## What is an Entity-Component-System?

Model development is an iterative process. For example, submodels are frequently changed or fully replaced by new or alternative versions. Further, different submodels may also need different state variables.

A design approach that facilitates this style of work is the Entity–Component–System (ECS) pattern, by decoupling the state of the model from the logic. While it is rooted in game design, it has proven to be useful for IBMs.

In ECS terms, *entities* are abstract handles to refer to what we usually consider entities in the IBM world. For example in a predator-prey-model, each predator and each prey could be represented by an entity. In a livestock model, each individual animal, each herd, and each market could be represented by an entity.

Each entity has one or more *components* which contain the entity's state variables. What can happen with an entity in the model results from the composition of its components. To resume the livestock example, all three types of entities could have a location component, but only animals would have an age. The herds could e.g. have a component holding their trade partners.

What is called a submodel in modelling terms are *systems* in ECS. Systems contain the logic of the model and operate on a certain combination of components. Examples of this kind of logic could be:

* If something has a position and a velocity, it moves (i.e. changes position).
* If something has a velocity and a mass, is falls due to gravity (i.e. changes velocity).

Because systems are only concerned with certain components relevant for their logic, while further components are ignored, the ECS approach enables independent development of different aspects of a model.

Component composition can also change over time. Composition changes are often performed by systems, and affect which systems process an entity.

A hurdle to overcome when starting to use ECS is to replace thinking about the type of an entity by considering which components/state variables are affected by which systems/processes. The model dynamics then emerge from the interaction of systems via overlapping components.

## Model purpose

The purpose of this model is to demonstrate the use of the ECS approach for individual-based models.

## Entities, state variables and scales

> In an ECS, the entities are indistinct, but characterized by the components they possess. Components contain an entity's state variables.

**Components**

In this grassing model, the only entities are grassers. All grassers possess the components `Position`, and `Energy`. An optional component `Movement` determines the current activity of each grasser.

Component `Position` contains continuous coordinates in a two-dimensional world.

```java
//- file:src/main/java/grassing/comp/Position.java
package grassing.comp;

public class Position extends com.artemis.Component {
    // Components must extend class Component

    public float x;
    public float y;

    public Position() {}
    public Position(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
```

Component `Energy` contains the entity's current energy budget.

```java
//- file:src/main/java/grassing/comp/Energy.java
package grassing.comp;

public class Energy extends com.artemis.Component {
    public float value;

    public Energy() {}
    public Energy(float value) {
        this.value = value;
    }
}
```

Component `Movement` contains the direction the entity is moving, in radians.

Entities have component `Movement` only when searching, but not when grassing. The presence or absence of the component defines an entity's behaviour.

```java
//- file:src/main/java/grassing/comp/Movement.java
package grassing.comp;

public class Movement extends com.artemis.Component {
    public float direction;

    public Movement() {}
    public Movement(float direction) {
        this.direction = direction;
    }
}
```

**Resources**

> Resources are objects that exist only once in the model, contrary to entities, and are potentially accessible for all submodels. This is kind of an optimization, it would also be possible to use one-of-a-kind entities instead.

Grassers live in a two-dimensional world. The world is represented by a grid of grass that grassers can consume.

```java
//- file:src/main/java/grassing/res/Grass.java
package grassing.res;

import grassing.util.FloatGrid;

public class Grass {
    // Any instance of a Java class can be a resource, but only one instance of each class is possible

    final public FloatGrid grass;

    public Grass(int width, int height, float initial) {
        this.grass = new FloatGrid(width, height);
        for (int i = 0; i < grass.length; i++) {
            grass.set(i, initial);
        }
    }
}
```

A single global pseudo random number generator is used for all processes that include randomness.

```java
//- file:src/main/java/grassing/res/Randomness.java
package grassing.res;

import java.util.Random;

public class Randomness {

    public final Random rng;

    public Randomness(long seed) {
        this.rng = new Random(seed);
    }
}
```

Spatial and temporal scales of the model are arbitrary. The duration of a model step is by orders of magnitude shorter than the average lifespan of a grasser. Grid cells are small enough to be rapidly traversed by a grasser.

## Process overview and scheduling

Processes in the model are grass growth, grasser metabolism, grasser reproduction and the two grasser behaviours grassing and searching. Processes are executed in that order.

> During initialization, systems are added to the world in a certain order, which defines the order of their execution. As systems interact indirectly via components, it is important to understand how the order of execution shapes the resulting dynamics.

```java
//- file:src/main/java/grassing/Main.java
package grassing;

import com.artemis.World;
import com.artemis.WorldConfigurationBuilder;
import grassing.comp.*;
import grassing.res.*;
import grassing.sys.*;
import grassing.util.MathUtil;

import java.util.Random;

public class Main {

    // ==> Parameters.

    public static void main(String[] args) throws InterruptedException {
        var setup = new WorldConfigurationBuilder()
                .with(new LogisticGrassGrowth(GRASS_GROWTH_RATE))
                .with(new Metabolism(ENERGY_CONSUMPTION))
                .with(new Reproduction(REPRODUCTION_PROB))
                .with(new GrassingBehaviour(GRASS_START_SEARCHING, GRASS_CONSUMPTION))
                .with(new SearchBehaviour(GRASS_START_GRASSING, RANDOM_WALK_ANGLE, RANDOM_WALK_SPEED))
                .with(new Graphics())
                .build()
                .register(new Randomness(0))
                .register(new Grass(WORLD_WIDTH, WORLD_HEIGHT, 0.8f));

        var world = new World(setup);

        createEntities(world, world.getRegistered(Randomness.class).rng);

        for (int i=0; i < 1000000; i++) {
            world.process();
            Thread.sleep(10);
        }
    }

    // ==> Create entities.
}
```

## Design details

Grassers consume the grass and convert it to energy. The grass regrows. Grassers have a metabolism consuming their energy. When energy drops to zero, a grasser dies. Grassers reproduce stochastically.

Grassers have two different behaviours: grassing and searching. Behaviour changes are based on grass availability in the current location.

## Initialization

The model is initialized with `NUM_GRASSERS` grassers. All grassers start with components `Position` and `Energy`. They start with the grassing behaviour, as they initially have no component `Movement`.

Position is initialized randomly. Energy is initialized with 1.0.

```java
//- Create entities
private static void createEntities(World world, Random rng) {
    for (int i = 0; i < NUM_GRASSERS; i++) {
        int entity = world.create();
        world.edit(entity)
                .add(new Position(rng.nextFloat() * WORLD_WIDTH, rng.nextFloat() * WORLD_HEIGHT))
                .add(new Energy(1f));
    }
}
```

## Submodels

Submodels are described in their order of execution.

> What the ODD calls submodels is called systems in the ECS world.

### Grass growth

Grass growth is logistic. The amount of grass is updated according

```math
p_{t+1} = p_t + r p_t \frac{k - p_t}{k}
```

where $`p_t`$ is the amount in the current step and $`p_{t+1}`$ in the next step. $`r`$ is the growth rate per step and $`k`$ is the capacity. Capacity is always 1.0.

> The grass growth system does not access any entities, and only modifies the `Grass` grid, which is a resource in ECS terms.
>
> A system implements what happens in each time step, but scheduling of those steps is done by the ECS, outside the systems.

```java
//- file:src/main/java/grassing/sys/LogisticGrassGrowth.java
package grassing.sys;

import com.artemis.BaseSystem;
import com.artemis.annotations.Wire;
import grassing.res.Grass;

public class LogisticGrassGrowth extends BaseSystem {
    // A system must extend BaseSystem or one of its many sub-classes that serve different purposes
    // Since this system does not access any entities, we simply extend `BaseSystem`

    @Wire Grass grass; // The resource is automatically "injected" by the ECS through @Wire

    private final float growthRate;
    private final float capacity;

    public LogisticGrassGrowth(float growthRate) {
        this.growthRate = growthRate;
        this.capacity = 1f;
    }

    @Override
    protected void processSystem() { // This method is called at every time step
        for(int i=0; i<grass.grass.length; i++) {
            float v = grass.grass.get(i);
            grass.grass.set(i, v + growthRate * v * (capacity - v) / capacity);
        }
    }
}
```

### Grasser metabolism

The grasser metabolism works on all entities that have the component `Energy`.

In each iteration, grassers lose `consumption` energy. When it's energy drops to zero, a grasser dies.

> Here, we are interested only in the `Energy` component which is modified in-place, thereby influencing the model evolution.

```java
//- file:src/main/java/grassing/sys/Metabolism.java
package grassing.sys;

import com.artemis.ComponentMapper;
import com.artemis.annotations.All;
import com.artemis.systems.IteratingSystem;
import grassing.comp.Energy;

@All(Energy.class) // Tells the system that is operates on entities that have all the listed components
public class Metabolism extends IteratingSystem {
    // We extend IteratingSystem, which carries out the work of iterating over the relevant entities

    // This is used in process(...) to access component Energy by entity id
    // The mapper is injected by Artemis automatically, so we don't need to construct it
    ComponentMapper<Energy> energy;

    private final float consumption;

    public Metabolism(float consumption) {
        this.consumption = consumption;
    }

    @Override
    protected void process(int id) { // This is automatically called at every model step, for every entity (id) matching the system's required component(s)
        Energy e = energy.get(id); // Get the component for the current entity
        e.value -= consumption;
        if(e.value <= 0) {
            world.delete(id);
        }
    }
}
```

### Grasser reproduction

The grasser reproduction works on all entities that have the components `Position` and `Energy`.

In each iteration, each grasser reproduces with a fixed probability.

When reproducing, energy is split in half between the grasser and it's offspring. The offspring starts with the grassing behaviour (i.e. without `Movement`).

> Here, we are interested only in entities that have components `Position` and `Energy`. Only component `Energy` is modified, while `Position` is required to determine the grasser's grid cell.

```java
//- file:src/main/java/grassing/sys/Reproduction.java
package grassing.sys;

import com.artemis.ComponentMapper;
import com.artemis.annotations.All;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import grassing.comp.*;
import grassing.res.Randomness;

@All({Position.class, Energy.class}) // Again, the combination of components we are interested in
public class Reproduction extends IteratingSystem {

    ComponentMapper<Position> position;
    ComponentMapper<Energy> energy;

    @Wire Randomness random;

    private final float reproductionProb;

    public Reproduction(float reproductionProb) {
        this.reproductionProb = reproductionProb;
    }

    @Override
    protected void process(int id) {
        if(random.rng.nextFloat() < reproductionProb) {
            Position pos = position.get(id);
            Energy e = energy.get(id);
            e.value /= 2;

            int entity = world.create();
            world.edit(entity)
                    .add(new Position(pos.x, pos.y))
                    .add(new Energy(e.value));

        }
    }
}
```

### Grassing

The grassing behaviour works on all entities that have the components `Position` and `Energy` but not `Movement`. Thus, it ignores grassers that are currently searching (and thus have `Movement`).

If the grasser's energy is 1.0 or above, it does nothing. Otherwise, the grasser decides if there is enough grass in its grid cell. If this is the case, the grasser consumes grass and converts it to energy. Otherwise, the grasser switches to the searching behaviour.

> The behaviour is switched by adding component `Movement`.
>
> Here, we want only entities that have components `Position` and `Energy`, but not `Movement`, which is optional according to our model logic. Thus, grassers that are searching instead of grassing will not be processed by this system. When the `Movement` component is added, this implies that the respective entity will not be processed by this system in the next step, but by the `SearchBehaviour`.

```java
//- file:src/main/java/grassing/sys/GrassingBehaviour.java
package grassing.sys;

import com.artemis.ComponentMapper;
import com.artemis.annotations.All;
import com.artemis.annotations.Exclude;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import grassing.comp.*;
import grassing.res.*;

@All({Position.class, Energy.class}) // Components of interest
@Exclude(Movement.class) // Here, we specify that we are only interested in entities that do not have component Movement
public class GrassingBehaviour extends IteratingSystem {

    ComponentMapper<Position> position;
    ComponentMapper<Energy> energy;

    @Wire Randomness random;
    @Wire Grass grass;

    private final float minGrass;
    private final float consumption;

    public GrassingBehaviour(float minGrass, float consumption) {
        this.minGrass = minGrass;
        this.consumption = consumption;
    }

    @Override
    protected void process(int id) {
        Energy e = energy.get(id);
        if(e.value >= 1) {
            return;
        }

        Position pos = position.get(id);
        float g = grass.grass.get((int) pos.x, (int) pos.y);
        if( g - consumption > minGrass ) {
            e.value += consumption;
            grass.grass.set((int) pos.x, (int) pos.y, g - consumption);
        } else {
            // Add component Movement and switch to search behaviour
            world.edit(id)
                .add(new Movement(random.rng.nextFloat() * 2 * (float) Math.PI));
        }
    }
}
```

### Search

The grassing behaviour system works on all entities that have the components `Position` and `Movement`. Thus, it ignores grassers that are currently grassing (and thus have no `Movement`).

The grasser decides if there is enough grass in it's grid cell. If this is the case, the grasser switches to the searching behaviour. Otherwise, it continues searching by a random walk. When reaching a world borders, the grasser is turned by 180°.

> The behaviour is switched by removing component `Movement`.

```java
//- file:src/main/java/grassing/sys/SearchBehaviour.java
package grassing.sys;

import com.artemis.ComponentMapper;
import com.artemis.annotations.All;
import com.artemis.annotations.Wire;
import com.artemis.systems.IteratingSystem;
import grassing.comp.*;
import grassing.res.Grass;
import grassing.res.Randomness;
import grassing.util.MathUtil;

@All({Position.class, Movement.class})
public class SearchBehaviour extends IteratingSystem {

    ComponentMapper<Position> position;
    ComponentMapper<Movement> movement;

    @Wire Grass grass;
    @Wire Randomness random;

    private final float minGrass;
    private final float maxAngle;
    private final float speed;

    public SearchBehaviour(float minGrass, float maxAngle, float speed) {
        this.minGrass = minGrass;
        this.maxAngle = maxAngle;
        this.speed = speed;
    }

    @Override
    protected void process(int id) {
        Position pos = position.get(id);

        float g = grass.grass.get((int) pos.x, (int) pos.y);
        if( g >= minGrass ) {
            // Remove component Movement to switch to grassing behaviour
            movement.remove(id);
        } else {
            randomWalk(id);
        }
    }

    private void randomWalk(int id) {
        Position pos = position.get(id);
        Movement mov = movement.get(id);

        mov.direction += (float) random.rng.nextGaussian() * maxAngle;

        var h = MathUtil.heading(mov.direction);
        float xNew = pos.x + h.x * speed;
        float yNew = pos.y + h.y * speed;
        if(grass.grass.contains((int) xNew, (int) yNew)) {
            pos.x = xNew;
            pos.y = yNew;
        } else {
            mov.direction = (mov.direction + (float) Math.PI) % (2 * (float) Math.PI);
        }
    }
}
```

## Parameters

The world has a size of 100 x 100 cells and starts populated with 1000 grassers.

```java
//- Parameters
final static int WORLD_WIDTH = 100;
final static int WORLD_HEIGHT = 100;

final static int NUM_GRASSERS = 1000;
```

Grass growth rate is 0.01.

```java
//- Parameters
final static float GRASS_GROWTH_RATE = 0.01f;
```

Grassers start searching when grass goes below 0.1 units, and resume grassing when finding grass of at least 0.2 units.

```java
//- Parameters
final static float GRASS_START_SEARCHING = 0.1f;
final static float GRASS_START_GRASSING = 0.2f;
```

Grassers can consume 0.05 units of grass per step, and need 0.02 units per step for their metabolism.

```java
//- Parameters
final static float GRASS_CONSUMPTION = 0.05f;
final static float ENERGY_CONSUMPTION = 0.02f;
```

Grassers have a reproduction probability of 5% per step.

```java
//- Parameters
final static float REPRODUCTION_PROB = 0.05f;
```

Grassers walk with 0.25 units per step, and turn with values from a normal distribution with a mean of 0° and a standard deviation of 30°.

```java
//- Parameters
final static float RANDOM_WALK_SPEED = 0.25f;
final static float RANDOM_WALK_ANGLE = MathUtil.deg2rad(30);
```

## References

Grimm V, Berger U, Bastiansen F, Eliassen S, Ginot V, Giske J, Goss-Custard J, Grand T, Heinz S, Huse G, Huth A, Jepsen JU, Jørgensen C, Mooij WM, Müller B, Pe’er G, Piou C, Railsback SF, Robbins AM, Robbins MM, Rossmanith E, Rüger N, Strand E, Souissi S, Stillman RA, Vabø R, Visser U, DeAngelis DL. 2006. **A standard protocol for describing individual-based and agent-based models**. *Ecological Modelling* 198:115-126.

Grimm V, Berger U, DeAngelis DL, Polhill G, Giske J, Railsback SF. 2010. **The ODD protocol: a review and first update**. *Ecological Modelling* 221: 2760-2768.

## Appendix

The @[Appendix](md/appendix.md) describes code that is not vital for understanding the model or the ECS concept.

* [Graphics](md/appendix.md#graphics)
* [Utilities](md/appendix.md#utilities)
* [Gradle files](md/appendix.md#gradle-files)
* [Git ignore file](md/appendix.md#git-ignore-file)
