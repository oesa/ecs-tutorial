# Appendix

This section describes code that is not vital for understanding the model or the ECS concept.

**Contents**

- [Graphics](#graphics)
- [Utilities](#utilities)
- [Gradle files](#gradle-files)
- [Git ignore file](#git-ignore-file)

## Graphics

The graphics system draws the amount of grass per cell by shades of green. Further, it draws searching grassers as orange triangles pointing in the direction of their heading, and grassing grassers as white rectangles.

```java
//- file:src/main/java/grassing/sys/Graphics.java
package grassing.sys;

import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.annotations.All;
import com.artemis.annotations.One;
import com.artemis.annotations.Wire;
import com.artemis.utils.IntBag;
import grassing.comp.*;
import grassing.res.Grass;
import grassing.util.FloatGrid;
import grassing.util.MathUtil;

import javax.swing.*;
import java.awt.*;

@All(Position.class)
public class Graphics extends BaseEntitySystem {

    protected ComponentMapper<Position> position;
    protected ComponentMapper<Movement> movement;

    @Wire Grass grass;

    private JPanel canvas;

    final private int CELL_SIZE = 6;

    public Graphics() {
        super();
    }

    @Override
    protected void initialize() {
        var frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        canvas = new JPanel() {
            @Override
            public void paint(java.awt.Graphics g) {
                Graphics.this.paint((Graphics2D)g);
            }
        };

        var dim = new Dimension(grass.grass.width * CELL_SIZE, grass.grass.height * CELL_SIZE);
        canvas.setPreferredSize( dim );

        frame.add(canvas);
        frame.pack();

        frame.setVisible(true);
    }

    @Override
    protected void processSystem() {
        canvas.paintImmediately(0, 0, grass.grass.width * CELL_SIZE, grass.grass.height * CELL_SIZE);
    }

    void paint(Graphics2D g2d) {
        FloatGrid grass = this.grass.grass;
        int s = CELL_SIZE;

        g2d.setBackground(Color.BLACK);

        for(int y=0; y<grass.height; y++) {
            for(int x=0; x<grass.width; x++) {
                g2d.setPaint(new Color(0f, 0.7f * grass.get(x, y), 0f));
                g2d.fillRect(x*s, y*s, s, s);
            }
        }

        int[] x = new int[3];
        int[] y = new int[3];
        IntBag grassers = subscription.getEntities();
        for (int i = 0; i < grassers.size(); i++) {
            int entity = grassers.get(i);
            var pos = position.get(entity);
            var mov = movement.get(entity);
            if(mov == null) {
                g2d.setPaint(Color.WHITE);
                g2d.fillRect((int) ((pos.x - 0.3f) * s), (int) ((pos.y - 0.3f) * s), (int) (0.6f * s), (int) (0.6f * s));
            } else {
                g2d.setPaint(Color.ORANGE);
                var f = MathUtil.heading(mov.direction);
                var r = MathUtil.heading(mov.direction + 0.5f * (float) Math.PI);
                x[0] = (int) ((pos.x + f.x) * s);
                y[0] = (int) ((pos.y + f.y) * s);
                x[1] = (int) ((pos.x + 0.4f * r.x) * s);
                y[1] = (int) ((pos.y + 0.4f * r.y) * s);
                x[2] = (int) ((pos.x - 0.4f * r.x) * s);
                y[2] = (int) ((pos.y - 0.4f * r.y) * s);
                g2d.fillPolygon(x, y, 3);
            }
        }
    }
}
```

## Utilities

The grid of `float` values for the grass.

```java
//- file:src/main/java/grassing/util/FloatGrid.java
package grassing.util;

public class FloatGrid {

    final public int width;
    final public int height;

    final public int length;

    protected final float[] data;

    public FloatGrid(int width, int height) {
        this.data = new float[width * height];
        this.width = width;
        this.height = height;

        this.length = width * height;
    }

    public final int getIndex(int x, int y) {
        return (y * width + x);
    }

    public final boolean contains(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }

    public final float get(int idx) {
        return data[idx];
    }
    public final float get(int x, int y) {
        return data[getIndex(x, y)];
    }
    public final void set(int idx, float value) {
        data[idx] = value;
    }
    public final void set(int x, int y, float value) {
        data[getIndex(x, y)] = value;
    }
}
```

Utility functions to handle angles.

```java
//- file:src/main/java/grassing/util/MathUtil.java
package grassing.util;

public abstract class MathUtil {

    public static Vector heading(float angle) {
        return new Vector((float) Math.cos(angle), (float) Math.sin(angle));
    }
    public static float deg2rad(float deg) {
        return (float) (deg * Math.PI / 180f);
    }
}
```

Vector for heading calculated from angle.

```java
//- file:src/main/java/grassing/util/Vector.java
package grassing.util;

public class Vector {
    public float x;
    public float y;

    public Vector(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
```

## Gradle files

[Gradle](https://gradle.org/) is used as build tool and for dependency management.

```groovy
//- file:build.gradle
apply plugin: 'application'

sourceCompatibility = 11
targetCompatibility = 11

repositories {
    mavenCentral()
}

dependencies {
    implementation group: 'net.onedaybeard.artemis', name: 'artemis-odb', version: '2.3.0'
}

application {
    getMainClass().set('grassing.Main')
}
```

```groovy
//- file:settings.gradle
rootProject.name = 'Grassing'
```

## Git ignore file

Regarding VCS/Git, we ignore files generated by Gradle, as well as build output.

```
//- file:.gitignore
.gradle
build
```
