# How to use this project

This projected is written via Literate Programming. Code is weaved into the Markdown documentation, and is extracted to a usable Java [Gradle](https://gradle.org/) project using the command line tool [Yarner](https://github.com/mlange-42/yarner).

There are several ways to use this projects or it's [downloadable outputs](https://git.ufz.de/oesa/ecs-tutorial/-/jobs/artifacts/master/download?job=build). We start from simply running downloaded executables, over building the downloaded Java project, to cloning the repository and running the complete chain from Yarner to execution.

After following any of the following sections, you should see a window with grassers bustling around.

**Contents**

* [Run from downloads](#run-from-downloads)
* [Browse the plain Java code](#browse-the-plain-java-code)
* [Build from downloads](#build-from-downloads)
* [Clone, extract, build](#clone-extract-build)

## Run from downloads

The [downloads](https://git.ufz.de/oesa/ecs-tutorial/-/jobs/artifacts/master/download?job=build) contain a file `Grassing.zip`. Extract it and navigate into folder `Grassing/bin`. Double-click or run file `Grassing` (or `Grassing.bat` if you are on Windows).

> This required [Java](https://www.java.com) 11 or newer to be installed on your machine. Simply try running the model, and install Java in case you get a respective error. Download Java [here](https://adoptopenjdk.net/).

## Browse the plain Java code

The [downloads](https://git.ufz.de/oesa/ecs-tutorial/-/jobs/artifacts/master/download?job=build) contain a folder `code`. This folder contains the project's Java code that you can browse following the structure of a "normal" Java project, rather than weaved into the Literate Programming document.

To work on the code, you can import the folder (potentially after renaming it) into your favourite Java-supporting IDE. If your IDE supports Gradle, you can import it as a Gradle project.

## Build from downloads

The [downloads](https://git.ufz.de/oesa/ecs-tutorial/-/jobs/artifacts/master/download?job=build) contain a folder `code`. Besides the project's Java code it contains a [Gradle](https://gradle.org/) project.

You can build and run the project with the following command executed inside folder `code`:

```
gradlew run
```

> This requires the Java JDK 11 or newer to be installed on your machine. Download Java [here](https://adoptopenjdk.net/).

## Clone, extract, build

Clone the repository using the following command:

```
git clone https://git.ufz.de/oesa/ecs-tutorial.git
```

> This required [Git](https://git-scm.com/) to be installed on your machine. Download Git [here](https://git-scm.com/downloads).

Navigate into the project and run [Yarner](https://github.com/mlange-42/yarner):

```
cd ecs-tutorial
yarner
```

> This requires [Yarner](https://github.com/mlange-42/yarner) to be installed on your machine. Download Yarner [here](https://github.com/mlange-42/yarner/releases/), or install using `cargo` if you have Rust installed:
> ```
> cargo install --git https://github.com/mlange-42/yarner yarner
> ```
> 
> Yarner needs to be on the PATH, or you need use the full path to Yarner:
> 
> ```
> C:/path/to/yarner/yarner
> ```

This will generate Java sources and a [Gradle](https://gradle.org/) project in sub-folder `code`. Navigate into it and run the project:

```
cd code
gradlew run
```

> This requires the Java JDK 11 or newer to be installed on your machine. Download Java [here](https://adoptopenjdk.net/).
